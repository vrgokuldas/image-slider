Firstly , its is an improved version of Banner-Slider in this link 
https://github.com/saeedsh92/Banner-Slider

But this repo( in link) needs an improvement

 In Banner-Slider,  we want to add image url manually in adapter class inside the switch case.
 
 The better case than that is, if we can add all( > 3 ) image urls to a single list and pass to the Image Slider.
 
 That's I am done Here
 
 Clone Banner Slider From 
 implementation 'com.ss.bannerslider:bannerslider:2.0.0'
 
 copy the following things
 
 Layout file Loom Like 
 
 
 <?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout
        xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:tools="http://schemas.android.com/tools"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        tools:context=".MainActivity">
    <ss.com.bannerslider.Slider
            android:id="@+id/imageSlider"
            android:layout_width="match_parent"
            android:layout_height="300dp"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
    />

</androidx.constraintlayout.widget.ConstraintLayout>
 
 
/* first */

import android.content.Context
import android.widget.ImageView
import com.squareup.picasso.Picasso
import ss.com.bannerslider.ImageLoadingService

class PicassoImageLoadingService(val context: Context) : ImageLoadingService {

    override fun loadImage(url: String?, imageView: ImageView?) {
        Picasso.with(context).load(url).into(imageView)
    }

    override fun loadImage(resource: Int, imageView: ImageView?) {
        Picasso.with(context).load(resource).into(imageView)
    }

    override fun loadImage(url: String?, placeHolder: Int, errorDrawable: Int, imageView: ImageView?) {
        Picasso.with(context).load(url).placeholder(placeHolder).error(errorDrawable).into(imageView)
    }
}



/* second */


import ss.com.bannerslider.adapters.SliderAdapter
import ss.com.bannerslider.viewholder.ImageSlideViewHolder

class MainSliderAdapter : SliderAdapter() {

    private var dataList = mutableListOf<String>()

    fun setData(list: MutableList<String>) {
        dataList = list
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindImageSlide(position: Int, imageSlideViewHolder: ImageSlideViewHolder?) {
        imageSlideViewHolder!!.bindImageSlide(dataList[position])
    }
}

/* Third In your Main activity call this function (imageSlider is the id of slider in the layout) */


 private fun setUpImageSLider() {

        Slider.init(PicassoImageLoadingService(this@MainActivity))
        val listImageURLs = mutableListOf(
            "https://assets.materialup.com/uploads/dcc07ea4-845a-463b-b5f0-4696574da5ed/preview.jpg",
            "https://assets.materialup.com/uploads/dcc07ea4-845a-463b-b5f0-4696574da5ed/preview.jpg",
            "https://assets.materialup.com/uploads/dcc07ea4-845a-463b-b5f0-4696574da5ed/preview.jpg",
            "https://assets.materialup.com/uploads/dcc07ea4-845a-463b-b5f0-4696574da5ed/preview.jpg",
            "https://assets.materialup.com/uploads/dcc07ea4-845a-463b-b5f0-4696574da5ed/preview.jpg"
        )

        val adapter = MainSliderAdapter()
        adapter.setData(listImageURLs)
        imageSlider.setAdapter(adapter)

        imageSlider.setLoopSlides(true)
        /**  set loop, first slide come after last slide **/

        imageSlider.setInterval(5000)
        /**  set interval for loop **/

        imageSlider.setSelectedSlide(1)
        /**  set default select slide on every launch **/

        imageSlider.setAnimateIndicators(true)
        /**  set animation indicators **/

        imageSlider.setIndicatorSize(12)
        /**  set animation indicators size **/

        /**  set click listerner , return position on click  **/
        imageSlider.setOnSlideClickListener { position ->
            Log.i("SliderProject>>>>>>>>>", "Pos = $position")
        }
    }
	
	
	DONT FORGET TO ADD INTERNET PERMISSION IN MANIFEST FILE
	
	 <uses-permission android:name="android.permission.INTERNET"/>