package com.vrg.imageslider

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import ss.com.bannerslider.Slider

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setUpImageSLider()
    }

    private fun setUpImageSLider() {

        Slider.init(PicassoImageLoadingService(this@MainActivity))
        val listImageURLs = mutableListOf(
            "https://assets.materialup.com/uploads/dcc07ea4-845a-463b-b5f0-4696574da5ed/preview.jpg",
            "https://assets.materialup.com/uploads/dcc07ea4-845a-463b-b5f0-4696574da5ed/preview.jpg",
            "https://assets.materialup.com/uploads/dcc07ea4-845a-463b-b5f0-4696574da5ed/preview.jpg",
            "https://assets.materialup.com/uploads/dcc07ea4-845a-463b-b5f0-4696574da5ed/preview.jpg",
            "https://assets.materialup.com/uploads/dcc07ea4-845a-463b-b5f0-4696574da5ed/preview.jpg"
        )

        val adapter = MainSliderAdapter()
        adapter.setData(listImageURLs)
        imageSlider.setAdapter(adapter)

        imageSlider.setLoopSlides(true)
        /**  set loop, first slide come after last slide **/

        imageSlider.setInterval(5000)
        /**  set interval for loop **/

        imageSlider.setSelectedSlide(1)
        /**  set default select slide on every launch **/

        imageSlider.setAnimateIndicators(true)
        /**  set animation indicators **/

        imageSlider.setIndicatorSize(12)
        /**  set animation indicators size **/

        /**  set click listerner , return position on click  **/
        imageSlider.setOnSlideClickListener { position ->
            Log.i("SliderProject>>>>>>>>>", "Pos = $position")
        }
    }
}
