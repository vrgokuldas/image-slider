package com.vrg.imageslider

import ss.com.bannerslider.adapters.SliderAdapter
import ss.com.bannerslider.viewholder.ImageSlideViewHolder

class MainSliderAdapter : SliderAdapter() {

    private var dataList = mutableListOf<String>()

    fun setData(list: MutableList<String>) {
        dataList = list
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindImageSlide(position: Int, imageSlideViewHolder: ImageSlideViewHolder?) {
        imageSlideViewHolder!!.bindImageSlide(dataList[position])
    }
}